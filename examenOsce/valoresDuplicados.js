const http = require("http");

const host = 'localhost';
const port = 8000;

function mergeAndSortArrays(array1, array2) {
    // Combina los dos arrays y elimina los elementos duplicados
    const mergedArray = [...new Set([...array1, ...array2])];
  
    // Ordena el array resultante de forma ascendente
    mergedArray.sort((a, b) => a - b);
  
    return mergedArray;
  }
  
  // Ejemplos de uso:
  const result1 = mergeAndSortArrays([1, 2, 3], [3, 4, 5]);
  console.log(result1); // Output: [1, 2, 3, 4, 5]
  
  const result2 = mergeAndSortArrays([-10, 22, 333, 42], [-11, 5, 22, 41, 42]);
  console.log(result2); // Output: [-11, -10, 5, 22, 41, 42, 333]