const express = require('express');
const mongoose = require('mongoose');
const multer = require('multer');
const cors = require('cors');

const app = express();
const port = process.env.PORT || 3000;

// Configurar middleware para aceptar datos JSON
app.use(express.json());

// Configurar middleware para permitir CORS
app.use(cors());

// Configurar conexión a la base de datos MongoDB
mongoose.connect('mongodb://localhost:27017/file_db', {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});
const db = mongoose.connection;

db.on('error', (error) => console.error('Error al conectar a MongoDB:', error));
db.once('open', () => console.log('Conexión exitosa a MongoDB'));

// Definir el esquema y modelo para los documentos de la base de datos
const fileSchema = new mongoose.Schema({
  content: String,
});

const File = mongoose.model('File', fileSchema);

// Configurar multer para subir archivos
const storage = multer.memoryStorage();
const upload = multer({ storage: storage });

// Ruta POST para subir un archivo
app.post('/uploadFile', upload.single('file'), async (req, res) => {
  try {
    if (!req.file) {
      return res.status(400).json({ message: 'Archivo no encontrado' });
    }

    const content = req.file.buffer.toString();
    const file = new File({ content });
    await file.save();

    return res.status(201).json({ message: 'Archivo subido correctamente', fileId: file._id });
  } catch (error) {
    return res.status(500).json({ message: 'Error al subir el archivo', error: error.message });
  }
});

// Ruta GET para obtener información por ID
app.get('/getInfo/:id', async (req, res) => {
  try {
    const fileId = req.params.id;
    const file = await File.findById(fileId);

    if (!file) {
      return res.status(404).json({ message: 'Archivo no encontrado' });
    }

    return res.status(200).json({ content: file.content });
  } catch (error) {
    return res.status(500).json({ message: 'Error al obtener la información', error: error.message });
  }
});

// Iniciar el servidor
app.listen(port, () => {
  console.log(`Servidor corriendo en http://localhost:${port}`);
});